# Open Study College test

To install the project

```sh
$ sudo nano /etc/apache2/sites-avalaible/openstudycollege-back.conf
```

```sh
<VirtualHost *:80>
        ServerName openstudycollege-back.local

        DocumentRoot /path/to/project/back

        <Directory /path/to/project/back>
                AllowOverride All
                Require all granted
        </Directory>

        ErrorLog /path/to/project/back/error.log
        CustomLog /path/to/project/back/access.log combined
</VirtualHost>
```
Enable the new virtual host

```sh
$ sudo a2ensite openstudycollege-back.conf
```

In the host file create the new url

```sh
$ sudo nano /etc/hosts
```

And write this line

```sh
127.0.0.1       openstudycollege-back.local
```

Now you can open the first test file in your navigator. The first test is in public/exercise1.html

To execute the second exercise you need MongoDB.
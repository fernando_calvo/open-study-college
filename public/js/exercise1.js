exercise1 = {
    str_reverse: function () {
        var strInput = document.getElementsByName('string')[0];
        var output = document.getElementById('output');
        var errorOutput = document.getElementById('error-output');
        output.innerHTML = '';
        errorOutput.innerHTML = '';
        OSC.backGet("exercise1/index.php?string=" + strInput.value)
                .then(function (response) {
                    output.innerHTML = response.string_reverse;
                })
                .catch(function (response) {
                    errorOutput.innerHTML = response.error;
                });
    }
};
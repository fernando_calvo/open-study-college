exercise2 = {
    items: [],
    get_items: function () {
        OSC.backGet("exercise2/index.php")
                .then(function (response) {
                    this.items = response.items;
                    this.print_items();
                }.bind(this))
                .catch(function (response) {
                    console.error(response);
                });
    },
    print_items: function() {
        var table = document.getElementById('todoitems');
        table.innerHTML = '';
        this.items.forEach(function(item) {
            var row = table.insertRow(-1);
            var titleCell = row.insertCell(-1);
            var titleInput = document.createElement('INPUT');
            titleInput.setAttribute('type', 'text');
            titleInput.setAttribute('name', 'title');
            titleInput.setAttribute('value', item.title);
            titleInput.setAttribute('item_id', item._id);
            titleCell.appendChild(titleInput);
            titleInput.addEventListener('focusout', this.update_item.bind(this));
            
            var doneCell = row.insertCell(-1);
            var doneInput = document.createElement('INPUT');
            doneInput.setAttribute('type', 'checkbox');
            doneInput.setAttribute('name', 'done');
            doneInput.setAttribute('item_id', item._id);
            doneInput.checked = item.done;
            doneInput.addEventListener('change', this.update_item.bind(this));
            doneCell.appendChild(doneInput);
            
            var removeCell = row.insertCell(-1);
            var removeButton = document.createElement('BUTTON');
            removeButton.setAttribute('item_id', item._id);
            removeButton.innerHTML = 'Remove';
            removeButton.addEventListener('click', this.remove_item.bind(this));
            removeCell.appendChild(removeButton);
        }.bind(this));
    },
    add_item: function() {
        var titleInput = document.getElementById('title-new');
        var doneInput = document.getElementById('done-new');
        var item = {
            title: titleInput.value,
            done: doneInput.checked
        };
        
        titleInput.value = '';
        doneInput.checked = false;
        
        this.items.push(item);
        this.save_item(item);
        this.print_items();
    },
    remove_item: function(event) {
        var target = event.target;
        var item = this.items.find(function(item) {
            return item._id === target.getAttribute('item_id');
        }.bind(this));
        
        OSC.backDelete('exercise2/index.php?id=' + item._id)
                .then(function() {
                    this.get_items();
                }.bind(this))
        console.log('remove', item)
    },
    update_item: function(event) {
        var target = event.target;
        var item = this.items.find(function(item) {
            return item._id === target.getAttribute('item_id');
        }.bind(this));
        
        if (target.getAttribute('name') === 'title') {
            item.title = target.value;
        } else if (target.getAttribute('name') === 'done') {
            item.done = target.checked;
        }
        this.save_item(item);
    },
    save_item: function(item) {
         OSC.backPost("exercise2/index.php?id=" + (item._id || ''), item)
                .then(function (response) {
                    console.log('guardado correctamente', response)
                })
                .catch(function (response) {
                    console.log('error', response)
                });
    }
};
exercise2.get_items();
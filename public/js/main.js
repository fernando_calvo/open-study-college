window.OSC = {
    config: {
        backUrl: "http://openstudycollege-back.local/"
    },
    backGet: function (url) {
        var promise = new Promise(
                function (resolve, reject) {
                    var xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function () {
                        if (xhttp.readyState == 4 && xhttp.status == 200) {
                            var response = JSON.parse(xhttp.responseText);
                            if (response.error) {
                                reject(response)
                            } else {
                                resolve(response);
                            }
                        }
                    };
                    xhttp.open("GET", OSC.config.backUrl + url, true);
                    xhttp.send();
                }
        )
        return promise;
    },
    backPost: function (url, data) {
        var promise = new Promise(
                function (resolve, reject) {
                    var xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function () {
                        if (xhttp.readyState == 4 && xhttp.status == 200) {
                            var response = JSON.parse(xhttp.responseText);
                            if (response.error) {
                                reject(response)
                            } else {
                                resolve(response);
                            }
                        }
                    };
                    xhttp.open("POST", OSC.config.backUrl + url, true);
                    xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                    xhttp.send(JSON.stringify(data));
                }
        );
        return promise;
    },
    backDelete: function (url) {
        var promise = new Promise(
                function (resolve, reject) {
                    var xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function () {
                        if (xhttp.readyState == 4 && xhttp.status == 200) {
                            var response = JSON.parse(xhttp.responseText);
                            if (response.error) {
                                reject(response)
                            } else {
                                resolve(response);
                            }
                        }
                    };
                    xhttp.open("DELETE", OSC.config.backUrl + url, true);
                    xhttp.send();
                }
        );
        return promise;
    }
}
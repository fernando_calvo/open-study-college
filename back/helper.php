<?php

function get_db_con() {
    $mongo = new MongoClient("mongodb://localhost:27017/openstudycollege");
    $db = $mongo->openstudycollege;
    return $db;
}

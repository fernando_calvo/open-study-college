<?php

require '../helper.php';

class Item {
    public $title, $done;
    private $_id;
    
    public function __construct($title = null, $done = false, $id = null) {
        $this->title = $title;
        $this->done = $done;
        if ($id !== null && $id !== '') {
            $this->_id = new MongoId($id);
        }
    }
    
    public function get_id() {
        if ($this->_id) {
            return $this->_id;
        } else {
            return false;
        }
    }
    
    public function save() {
        $db = get_db_con();
        if ($this->_id) {
            $item_to_save = $this->toArray();
            $db_result = $db->todo_items->save($item_to_save);
        } else {
            $this->_id = new MongoId();
            $db_result = $db->todo_items->save($this->toArray());
        }
        return $db_result;
    }
    
    public function toArray() {
        $json = array(
            'title' => $this->title,
            'done' => $this->done
        );
        if ($this->_id) {
            $json['_id'] = $this->_id->{'$id'};
        }
        return $json;
    }
    
    public function getItem($id) {
        $db = get_db_con();
        $this->_id = new MongoId($id);
        $db_result = $db->todo_items->findOne(array('_id' => $this->_id));

        if (!$db_result) {
            $this->_id = null;
            return false;
        }

        $this->done = $db_result['done'];
        $this->title = $db_result['title'];
        return $this;
    }
    
    public function remove() {
        $db = get_db_con();
        $db_result = $db->todo_items->remove(array('_id' => $this->_id));
        
        return $db_result;
    }
    
    static function get_items() {
        $db = get_db_con();
        $db_result = $db->todo_items->find();
        
        $items = array();
        foreach ($db_result as $item_db) {
            $items[] = new Item($item_db['title'], $item_db['done'], $item_db['_id']);
        }
        
        return $items;
    }
}

<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header("Access-Control-Allow-Methods: GET,HEAD,OPTIONS,POST,PUT,DELETE");

require './item.php';

$method = $_SERVER['REQUEST_METHOD'];

switch ($method) {
    case 'POST':
        save_item();
        break;
    case 'GET':
        get_items();
        break;
    case 'DELETE':
        remove_item();
        break;
    default:
        break;
}

function save_item() {
    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);
    $title = key_exists('title', $data) ? $data->title : null;
    $done = key_exists('done', $data) ? $data->done : false;
    $id = key_exists('id', $_GET) ? $_GET['id']: null;
    if (!$title) {
        echo json_encode(array(
            "error" => "Title missed"
        ));
        return;
    }
    $item = new Item($title, $done, $id);
    $result = $item->save();
    if ($result['ok']) {
        echo json_encode(array(
            "item" => $item->toArray()
        ));
    }
}

function remove_item() {
    $id = $_GET['id'];
    
    if (!$id) {
        echo json_encode(array(
            "error" => "Id missed"
        ));
        return;
    }
    
    $item_removed = new Item();
    $item_removed->getItem($id);
    
    if (!$item_removed->get_id()) {
        echo json_encode(array(
            "result" => false,
            "error" => 'Item not found'
        ));
        return;
    }
    $result = $item_removed->remove();
    
    if ($result['ok']) {
        echo json_encode(array(
            "result" => true,
            "item_removed" => $item_removed
        ));
    } else {
        echo json_encode(array(
            "result" => false,
            "error" => $result['err']
        ));
    }
}

function get_items() {
    $items = Item::get_items();
    
    $itemsArr = array();
    foreach ($items as $item) {
        $itemsArr[] = $item->toArray();
    }
    echo json_encode(array(
        'result' => true,
        'items' => $itemsArr
    ));
}
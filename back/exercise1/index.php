<?php

header('Access-Control-Allow-Origin: *');

$string = $_GET['string'];

if (!$string) {
    echo json_encode(array("error" => 'String param missed'));
    exit();
}
for($i=strlen($string)-1, $j=0; $j<$i; $i--, $j++) {
    list($string[$j], $string[$i]) = array($string[$i], $string[$j]);
}
echo json_encode(array(
    "string_reverse" => $string
));